# Put those two alias in you .bashrc file
# alias USE_LESS='./useless.sh & USE_LESS_PID=$!'
# alias KILL_USE_LESS='kill $USE_LESS_PID'

# Warning: The less compiler (lessc) version installed by "apt-get install node-less" in Ubuntu 14.04,
# which is lessc 1.4.2 has a bug and throws a non-sense sintax error when importing
# external less files.
# Perform the following steps in order to avoid this issue by installing a newer version:

# sudo apt-get remove node-less
# sudo apt-get install npm
# sudo npm -g install nodejs
# sudo ln -s /usr/bin/nodejs /usr/bin/node
# sudo ln -s /home/matias/.npm/less/1.7.5/package/bin/lessc /usr/bin/lessc

# IMPORTANT: the version number (1.7.5) may differ and, of course, the username

# sudo npm install -g less


lessc static/css2/style.less > static/css2/style.css;


if [ "$1" == '-v' ]; then
    VERBOSE=true;
else
    VERBOSE=false;
fi

function log {
    if [ "$VERBOSE" = true ]; then
        echo $1;
    fi
}

while true; do

	should_compile=false;
	# for each file f at static/css2/ which ends with .less except for those ones inside bootstrap_less dir
	for f in $(find static/css2/ -name "*.less" | grep -v "bootstrap_less")
	do
		# last modification date
		last_mod=$(stat $f --format="%Y");
		# now
		now=$(date +"%s");
		# time since last modification
		offset=$(expr $now - $last_mod);
		# if file was modified less than 5 secs ago
		if [ "$offset" -le 2 ]; then
			should_compile=true;
            log "# $f  file changed!";
		fi
	done

	if [ "$should_compile" = true ]; then
		lessc static/css2/style.less > static/css2/style.css;
        log "# Compiled!";
    else
        log "# Done. Nothing to compile";
	fi

	sleep 1;

done