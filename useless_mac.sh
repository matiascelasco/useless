#! /bin/bash
echo
while true; do 

	should_compile=false;
	# for each file f at static/css2/ which ends with .less except for those ones inside bootstrap_less dir
	now=$(date +"%s");
	for f in $(find static/css2 -name "*.less" | grep -v "bootstrap_less")
	do
		# last modification date
		last_mod=$(stat -r $f | cut -d " " -f 10);
		# time since last modification
		offset=$(expr $now - $last_mod);
		if [ $offset -le 5 ]; then
			echo "$(date): el archivo $f cambio!"
			should_compile=true;
		fi
	done

	if [ "$should_compile" = true ]; then
		echo
		echo "A compilar se ha dicho"...;
		echo
		echo
		lessc static/css2/style.less > static/css2/style.css;
	fi

	sleep 1;

done
